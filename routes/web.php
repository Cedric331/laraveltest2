<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('accueil');
});

Route::get('/ajout', function () {
    return view('ajout');
});

Route::post('/ajout', function () {
        DB::insert('insert into superheroes (nom, degats, vie) values (:nom, :degats, :vie)' , [
        ':nom' => request()->input('nom'),
        ':degats' => request()->input('degats'),
        ':vie' => request()->input('vie')
    ]);
    return redirect()->route('accueil');
});

Route::get('/liste', function () {
    // DB::insert('insert into superheroes (nom, degats, vie) values (:nom, :degats, :vie)' , [
    //     ':nom' => 'Spiderman',
    //     ':degats' => 20,
    //     ':vie' => 100
    // ]);
    $tableau = DB::table('superheroes')->get();
    return view('liste', [
        "superheroes" => $tableau
    ]);
})->name('accueil');
