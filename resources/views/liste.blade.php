<!-- Affichage dans un tableau -->
@extends('layout')

@section('content')
<div class=container>
    <table class="table table-dark">
          <thead>
          <tr>
             <th scope="col">#</th>
             <th scope="col">Nom</th>
             <th scope="col">Vie</th>
             <th scope="col">Dégâts</th>
             <th scope="col">Recherche</th>
             <th scope="col">Fiche</th>
             <th scope="col">Supprimer</th>
          </tr>
          </thead>
             <tbody>
@foreach ($superheroes as $ligne)

         <tr>
             <td>{{$ligne->id}}</td>
             <td>{{$ligne->nom}}</td>
             <td>{{$ligne->vie}}</td>
             <td>{{$ligne->degats}}</td>
             <td><a href=\"https://www.google.fr/search?sxsrf=ALeKk02XsxlGfxvsb3S0yzNiPGbCG02y6A%3A1595016169614&ei=6QMSX5-GJY-MlwTF7pJA&q=".$ligne['nom']."&oq=".$ligne['nom']."&gs_lcp=CgZwc3ktYWIQAzIFCAAQsQMyBAgAEEMyBQgAELEDMgIIADIICAAQsQMQgwEyBAgAEAoyBQgAELEDMgIIADICCAAyAggAOgQIABBHOgQIIxAnOgoIABCxAxAUEIcCOgcIIxDqAhAnOgcIABCxAxBDUIaTAlizpQJg9KYCaAFwAngCgAGIAYgB8QmSAQM1LjeYAQCgAQGqAQdnd3Mtd2l6sAEK&sclient=psy-ab&ved=0ahUKEwjfns2hitXqAhUPxoUKHUW3BAgQ4dUDCAw&uact=5\" target=\"_blank\"><i class=\"fas fa-search fa-lg\"></i></a></td>
             <td><a href=\"fiche/" .$ligne['id']."\">Voir</a></td>
             <td><a href=\"?delete=" .$ligne['id']. "\"><i class=\"fas fa-window-close fa-lg\"></i></a><hr></td>
          </tr>

 @endforeach
             </tbody>
    </table>
 </div>
 @endsection
 