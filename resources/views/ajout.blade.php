@extends('layout')

@section('content')
<!-- Formulaire d'ajout -->
<div class="container align-start centre wrap">
    <form action="" method="post">
        @csrf
       <div class="form-group">
          <label for="hero">Nom :</label>
          <input type="text" id="hero" name="nom" placeholder="Nom">

          <label for="degats">Dégâts :</label>
          <input type="number" id="degats" name="degats" placeholder="Degats">

          <label for="vie">Vie :</label>
          <input type="number" id="vie" name="vie" placeholder="Vie">

          <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle fa-lg"></i></button>
       </div>
    </form>
 </div>

 @endsection
